'use strict'
import express from 'express'
const api = express.Router()

import auth from './auth.js'
api.post('/login', auth.login)

export default api
