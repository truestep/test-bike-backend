function clearCookies(res) {
    res.clearCookie('authtoken')
    return res
}

function setCookies(res, authToken) {
    const oneDayToMilliSeconds = 24 * 60 * 60 * 1000
    res.cookie('authtoken', authToken, {
        maxAge: oneDayToMilliSeconds,
        httpOnly: true,
        secure: true,
        sameSite: 'none'
    })
}

async function login(req, res) {
    clearCookies(res)
    let {userId} = req.body
    const user = global.users[userId]
    if (!user) {
        return res.status(400).send({error: "no such user"})
    }
    console.log(`logging in ${userId}`)
    const authToken = `token-${userId}-${Date.now()}`
    console.log(`authenticated user ${userId}`)
    global.users[userId].assignToken(authToken)
    setCookies(res, authToken)
    return res.status(200).send({authenticated: true})
}

const authChecker = async (req, res, next) => {
    if (req.cookies?.authtoken == undefined) {
        res.status(401)
        return res.send("login first")
    } else {
        console.log(`authenticated request ${req.cookies?.authtoken}`)
        next()
    }
}
export default {
    login,
    authChecker
}