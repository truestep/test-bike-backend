import ExternalBicycleService from "../externalServices/bicycle.js"

class Bicycle {
    constructor (id) {
        this.id = id
        this.externalBicycle = new ExternalBicycleService(id)
        for (let obj = this; obj; obj = Object.getPrototypeOf(obj)){
            for (let name of Object.getOwnPropertyNames(obj)){
                if (typeof this[name] === 'function'){
                    this[name] = this[name].bind(this)
                }
            }
        }
    }

    id
    light = false
    status // free/busy/broken
    location
    ip
    externalBicycle

    async setLight(state) {
        if (state === "on") {
            await this.externalBicycleService.lightOn()
        } else {
            await this.externalBicycleService.lightOff()
        }
    }

    setStatus(status) {
        this.status = status
    }

    setLocation(location) {
        this.location = location
    }

    setIp(ip) {
        this.ip = ip
    }

    getStatus() {
        return {
            id,
            light,
            status,
            location,
            ip
        }
    }
}

global.bicycles = {
    "z18443": new Bicycle("z18443"),
}


export default Bicycle