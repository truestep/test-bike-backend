class User {
    constructor (name) {
        this.name = name
    }

    name
    funds = 0
    currentBike
    token

    addFunds(amount) {
        this.funds += Number(amount)
    }

    assignToken(token) {
        this.token = token

    }

    currentBike(bikeId) {
        this.currentBike = bikeId
    }

    static getUserByToken(token) {
        for (const [key, value] of Object.entries(global.users)) {
            console.log(`${key}: ${value}`)
            if (value.token === token) {
                return value
            }

        }
    }
}

global.users = {
    "a": new User("a"),
    "b": new User("b"),
    "c": new User("c"),
    "d": new User("d"),
}

export default User