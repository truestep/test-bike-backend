import axios from "axios"
import https from "https"

class BicycleService {
    constructor(bicycleId) {
        this.id = bicycleId
    }
    id

    unlock() {
        return this.execute('locked', {state:false})
    }
    lock() {
        return this.execute('locked', {state:true})
    }

    lightOff() {
        return this.execute('light', {state:false})
    }
    lightOn() {
        return this.execute('light', {state:true})
    }

    status(state) {
        return this.execute('light', {state})
    }

    getLocation() {
        return this.execute('location')
    }

    getCurrentState() {
        return this.execute('getstate')
    }

    getIp() {
        return this.execute('getip')
    }


    async execute(command, data, queryParams) {
        let url
        try {
            url = `https://localhost:${this.id}/api/${command}`
            const requestObj = {
                url,
                method: 'post',
                responseType: 'json',
                data,
                params: queryParams,
                headers: {},
                httpsAgent: new https.Agent({
                    rejectUnauthorized: false
                })
            }

            console.log(`calling ${url} with data ${JSON.stringify(requestObj)}`)
            let res = await axios(requestObj)
            console.log(`from ${url} received ${JSON.stringify(res.data)}`)
            return res.data
        } catch (e) {
            let error
            if (e.response) {
                let {status, statusText, headers, data} = e.response
                error = {
                    status, statusText, headers, data,
                    systemMessage: e.message
                }
                if (data && data.error) {
                    error.message = data.error
                }
            } else {
                error = {systemMessage: e.message}
            }
            console.error(`error calling ${url} error:${JSON.stringify(error)}`)
            return {
                error
            }
        }

    }
}

export default BicycleService
