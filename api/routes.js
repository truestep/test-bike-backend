'use strict'
import express from 'express'
const api = express.Router()

import auth from '../auth/auth.js'
import bicycles from './bicycles.js'
import users from './users.js'


api.use(auth.authChecker)
api.get('/users', users.listUsers)
api.post('/addfunds', users.addFunds)

api.post('/addbike', bicycles.addBicycle)
api.post('/rentabike', bicycles.rentaBicycle)
api.post('/turnlights', bicycles.turnAllBicyclesLights)


export default api
