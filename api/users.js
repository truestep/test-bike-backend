'use strict'

import User from "../services/user.js"

class UsersApi {
    constructor() {
    }

    async addFunds(req, res) {
        try {
            const token = req.cookies?.authtoken
            const user = User.getUserByToken(token)
            const amount = req.body.amount
            user.addFunds(amount)
            res.send({"total": user.funds})
        } catch (e) {
            console.error('status error ', e)
            res.status(500)
            res.send(e)
        }
    }

    async listUsers(req, res) {
        try {
            const users = global.users
            res.send(users)
        } catch (e) {
            console.error('status error ', e)
            res.status(500)
            res.send(e)
        }
    }

}

const usersApi = new UsersApi()

export default usersApi