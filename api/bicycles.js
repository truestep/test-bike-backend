'use strict'
import Bicycle from "../services/bicycle.js"
import BicycleService  from "../externalServices/bicycle.js"
import User from "../services/user.js"

class BicycleApi {
    constructor() {
    }

    async addBicycle(req, res) {
        try {
            global.bicycles[req.body.id] = new Bicycle(req.body.id)
            res.send()
        } catch (e) {
            console.error('status error ', e)
            res.status(500)
            res.send(e)
        }
    }

    async getAvailableBicycles(req, res) {
        try {
            res.send(global.bicycles)
        } catch (e) {
            console.error('status error ', e)
            res.status(500)
            res.send(e)
        }
    }

    async rentaBicycle(req, res) {
        const token = req.cookies?.authtoken
        const bikeId = req.body.bikeId
        const user = User.getUserByToken(token)
        if (user.funds > 10) {
            const bike = new BicycleService(bikeId)
            await bike.unlock()
        }
        try {
        } catch (e) {
            console.error('status error ', e)
            res.status(500)
            res.send(e)
        }
    }

    async returnBicycle(req, res) {
        try {

        } catch (e) {
            console.error('status error ', e)
            res.status(500)
            res.send(e)
        }
    }

    async turnAllBicyclesLights(req, res) {
        const {state} = req.body
        try {
            for (const [key, value] of Object.entries(global.bicycles)) {
                console.log(`${key}: ${value}`)
                value.setLight(state)
            }

        } catch (e) {
            console.error('status error ', e)
            res.status(500)
            res.send(e)
        }
    }

}

const bicycleApi = new BicycleApi()

export default bicycleApi